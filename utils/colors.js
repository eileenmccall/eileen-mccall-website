export default {
    dark: "#393939",
    javascript: "#EBC924",
    react: "#00d8ff",
    node: "#6cc24a",
    angular: "#b52e31"
};