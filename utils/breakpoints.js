export var breakpoints = {
    tablet: "@media (min-width: 375px)",
    laptop: "@media (min-width: 768px)",
    laptopL: "@media (min-width: 1024px)",
    desktop4K: "@media (min-width: 1440px)"
}

