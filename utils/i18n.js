export default {
    en: {
        lang: "en",
        menuHome: "Home",
        menuWork: "Work",
        menuAbout: "About",
        menuContact: "Contact",
        menuPossums: "Possums",
        menuClose: "Close",
        underConstructionText: "Heads up! I'm still building this site. Some stuff may not work right yet!",
        tagline: "Web Developer",
        introText: "Hi! My name’s Eileen, and I love making cool stuff on the web. Feel free to take a look around, take off your shoes and stay while.",
        introQuip: "You look great today, by the way.",
        praiseTitle: "Kind words from colleagues and clients.",
        workTitle: "Work",
        workSubtitle: "A collection of cool stuff I've built using my favorite technologies.",
        contactTitle: "Contact",
        contactSubtitle: "Got an idea for a project? Wanna hire me? Just wanna air your grievances? This is the page for you.",
        contactEmail: "Email",
        contactEmailPlaceholder: "Email",
        contactSubject: "Subject",
        contactSubjectPlaceholder: "Subject",
        contactMessage: "Message",
        contactMessagePlaceholder: "Message",
        contactSocialMessage: "If you're not a fan of HTML contact forms, feel free to reach out on social media.",
        contactOnSuccess: "Form submitted! I'll get back to you soon!",
        contactOnError: "Sorry! Something went wrong!",
        projectDetails: {
            timeToReadBefore: "",
            timeToReadAfter: "minute read"
        },
        about: {
            title: "About me",
            subtitle: "Everything you want to know me and more",
            timelineTitle: "Timeline for Context",
            timeline: [
                {
                    year: 1993,
                    items: [
                        "Born at Landstuhl Regional Medical Center in Landstuhl, Germany"
                    ]
                },
                {
                    year: 1997,
                    items: [
                        "Moved in with my grandparents in Mechanicsville, Iowa (population ~1,700)"
                    ]
                },
                { 
                    year: 2011,
                    items: [
                        "Graduated from North Cedar High School with a class of 55 students"
                    ]
                },
                {
                    year: 2015,
                    items: [
                        "Studied creative writing at Wartburg College, graduated with B.A.",
                        "Developed an obsession with the French language and the Canadian province of Québec, made decision to move there one day",
                        "Taught myself to play the guitar and mandolin"
                    ]
                },
                {
                    year: 2016,
                    items: [
                        `Moved to Des Moines, IA`,
                        "Working as an ad designer for Gannett Co.",
                        `Met my lifelong friend Lydia via our shared love of <a href="https://youtube.com/watch?v=FWCH_h8SEuo">Punch Brothers</a>`,
                        "Began teaching myself to code"
                    ]
                },
                {
                    year: 2017,
                    items: [
                        `Got my first coding job as a junior software engineer at <a href="https://wewritecode.com">We Write Code</a>`,
                        "Started HRT (Hormone Replacement Therapy)"
                    ]
                },
                {
                    year: 2018,
                    items: [
                        "Started living full-time as a woman",
                        "Spent a week visiting Lydia in Québec",
                        "Came out to my parents",
                        "Took TEF (Test d'évaluation de français) Canada"
                    ]
                },
                {
                    year: 2019,
                    items: [
                        `Left We Write Code to work at Fabulas, a web consultancy helmed by my good friend <a href="https://twitter.com/edelman215">Michael Edelman</a>`,
                        "Worked remotely in Québec for a month (easily the best month of my life up to that point)",
                        "Changed my name legally to Eileen",
                        `Left Fabulas, began working at <a href="https://dice.com">Dice</a>`,
                        "Canvassed for Bernie Sanders"
                    ]
                },
                {
                    year: 2020,
                    items: [
                        "Began process of changing my name and sex marker on my birth certificate (still waiting unfortunately, due to COVID)",
                        "Tested positive for COVID-19, recovered",
                        "Began taking piano lessons"
                    ]
                },
                {
                    year: 2021,
                    items: [
                        "Began process of changing name and sex marker on passport"
                    ]
                }
            ],
            sections: [
                {
                    title: "My life philosophy:",
                    items: [
                        `It's my opinion that the greatest philosopher who ever lived is Dolly Rebecca Parton, and it's from her that I took the thesis statement of my life: <b>"Find out who you are, then do it on purpose."</b> All of my life decisions are shaped by this one guiding principle.`,
                        `One of the blessings of being transgender is that I don't have the luxury of being content with the identity I was born into. I had to make decisions about who I was and what I wanted from the world.`,
                        `A thought experiment that I'm very fond of involves imagining yourself on your deathbed. You're at death's door, and you know the end is near. Then, magically, you are transported back in time to this exact moment. What do you do differently? What things are truly important to you? How would you live your life if you had a second chance?`
                    ]
                },
                {
                    title: "I'm obsessive",
                    items: [
                        `My brain is single-threaded. When I set my mind to a task, you can pretty much guarantee that it's going to happen whether it takes an hour, a week, or a year. I don't like leaving thing half-done, it gives me anxiety.`,
                        `Because of this, I tend to follow the axiom of <a href="https://sive.rs/hellyeah">"Hell yeah or no"</a>. I'm very picky about what projects I pick up and which ones I pass on, which means I often have to say no to things that I'd like very much to do. It sucks, but it's life. It also means, however, that the projects I do decide to take on tend to be fun, intellectually stimulating, and rewarding (sometimes financially, sometimes not).`,
                    ]
                },
                {
                    title: "I'm a(n aspiring) digital nomad",
                    items: [
                        `The happiest I've ever been is when I was working remotely full-time and could travel wherever and whenever I liked. The pandemic has unfortunately limited my ability to travel, and I have the sensation of being like a fly in a jar. It feels horrible to be wasting time sitting at home that could be spent seeing new places and meeting new people.`,
                        `I chose to work in tech specifically because of its location independence. My ideal life would be spent spending 6 months to a year in one place, and then picking up and going someplace completely different. Long enough to establish habits, make friends, become a regular at coffee shops and bars, and get to know a place deeply.`,
                        `One of my great passions in life is language learning, and part of what fuels my wanderlust is the urge to be surrounded by unfamiliar words and sounds.`
                    ]
                }
            ],
            otherThings: {
                title: "Other things about me",
                items: [
                    `<b>I'm an extroverted introvert.</b> I enjoy being around people, and I like being the center of attention when it's on my terms, but I require time alone in order to function.`,
                    `<b>I'm slow to anger.</b> I don't like being upset, it's exhausting.`,
                    `<b>I don't get along with bullshitters very well.</b> I make a point to surround myself with earnest, genuine people. I can't stand people who put on airs to try to impress you or get you to like them. Blow smoke up someone else's ass, would ya?`,
                    `<b>I'm very intentional about the things I allow into my life.</b> This includes everything: movies, books, music, objects, places, and people. Everything. This makes me seem snobbish to some people. I don't care. I put a lot of effort into searching out and consuming the best things I can. Every movie I watch and every album I listen to is carefully curated.`,
                    `<b>I'm vulgar.</b> I swear a lot, I make dick jokes, and I love good-natured shit-talking.`
                ]
            }
        }
    },
    fr: {
        lang: "fr",
        menuHome: "Accueil",
        menuWork: "Projets",
        menuAbout: "À propos",
        menuContact: "Contact",
        menuPossums: "Opossums",
        menuClose: "Fermer",
        underConstructionText: "Attention! Ce site est encore en construction. Certaines choses risque de ne pas encore fonctionner correctement!",
        tagline: "Développeuse Web",
        introText: "Salut! Je m'appelle Eileen, et j'adore créer des choses cool sur le web. Faites comme chez-vous, enlevez vos souliers, et détendez-vous.",
        introQuip: "En passant, j'adore votre tenue.",
        praiseTitle: "Des mots fins de collègues et de clients.",
        workTitle: "Projets",
        workSubtitle: "Une collection de projets cool que j'ai bâtis avec mes outils préférés.",
        contactTitle: "Contact",
        contactSubtitle: "Vous avez une idée pour un projet? Vous voulez m'engager? Cette page est faite pour vous!",
        contactEmail: "E-mail",
        contactEmailPlaceholder: "E-mail",
        contactSubject: "Sujet",
        contactSubjectPlaceholder: "Sujet",
        contactMessage: "Message",
        contactMessagePlaceholder: "Message",
        contactSocialMessage: "Si vous préférez, vous pouvez aussi me contacter sur les réseaux sociaux!",
        contactOnSuccess: "Merci! Je vous répondrai bientôt!",
        contactOnError: "Désolée! Une erreur s'est produite!",
        projectDetails: {
            timeToReadBefore: "lecture de",
            timeToReadAfter: "minutes"
        },
        about: {
            title: "À propos",
            subtitle: "Tout ce que vous voulez savoir sur moi et plus encore.",
            timelineTitle: "Chronologie pour contexte",
            timeline: [
                {
                    year: 1993,
                    items: [
                        "Je suis née au centre médical régional de Landstuhl à Landstuhl, Allemagne"
                    ]
                },
                {
                    year: 1997,
                    items: [
                        "J'emménage avec mes grands-parents à Mechanicsville, Iowa (population ~ 1700)"
                    ]
                },
                { 
                    year: 2011,
                    items: [
                        "Je suis diplômée de l'école secondaire North Cedar avec une classe de 55 élèves"
                    ]
                },
                {
                    year: 2015,
                    items: [
                        "J'étudie l'écriture créative au Wartburg College et a obtenu un baccalauréat ès arts.",
                        "Je développe une obsession pour la langue française et le Québec, et j'ai décidé de s'y installer un jour",
                        "J'apprends à jouer de la guitare et de la mandoline"
                    ]
                },
                {
                    year: 2016,
                    items: [
                        `Je déménage à Des Moines, IA`,
                        "Je travaille en tant que concepteur d'annonces pour Gannett Co.",
                        `Je rencontre mon amie de toujours Lydia grâce à notre amour partagé pour <a href="https://youtube.com/watch?v=FWCH_h8SEuo">Punch Brothers</a>`,
                        "Je commence à m'apprendre à coder"
                    ]
                },
                {
                    year: 2017,
                    items: [
                        `J'obtiens mon premier emploi de codeur en tant qu'ingénieur logiciel junior chez <a href="https://wewritecode.com"> We Write Code </a>`,
                        "je commence à prendre du THS (traitement hormonal substitutif)"
                    ]
                },
                {
                    year: 2018,
                    items: [
                        "Je commence à vivre à temps plein en tant que femme",
                        "Je passe une semaine à visiter Lydia au Québec",
                        "Je fais mon coming-out à mes parents",
                        "Je passe le TEF (Test d'évaluation de français) Canada"
                    ]
                },
                {
                    year: 2019,
                    items: [
                        `Je quitte We Write Code pour travailler chez Fabulas, un cabinet de conseil en ligne dirigé par mon bon ami <a href="https://twitter.com/edelman215">Michael Edelman</a>`,
                        "Je travaille à distance au Québec pendant un mois (facilement le meilleur mois de ma vie jusque-là)",
                        "J'ai changé légalement mon nom en Eileen",
                        `Je quitte Fabulas, je commence à travailler chez <a href="https://dice.com">Dice</a>`,
                        "Je fais du démarchage électoral pour Bernie Sanders"
                    ]
                },
                {
                    year: 2020,
                    items: [
                        "Je commence le processus de changement de mon nom et de mon marqueur de sexe sur mon certificat de naissance (toujours en attente malheureusement, en raison du COVID)",
                        "Je teste positive pour COVID-19, je récupére",
                        "Je commence à prendre des cours de piano"
                    ]
                },
                {
                    year: 2021,
                    items: [
                        "Je commence le processus de changement de nom et de sexe sur le passeport"
                    ]
                }
            ],
            sections: [
                {
                    title: "Ma philosophie de vie:",
                    items: [
                        `J'estime que la plus grande philosophe qui ait jamais vécu est Dolly Rebecca Parton, et c'est d'elle que j'ai pris la thèse de ma vie: <b> "Découvrez qui vous êtes, puis faites-le exprès." </b> Toutes mes décisions de vie sont façonnées par ce principe directeur unique.`,
                        `Une des bénédictions d'être transgenre est que je n'ai pas le luxe de me contenter de l'identité dans laquelle je suis né. Je devais prendre des décisions sur qui j'étais et ce que je voulais du monde.`,
                        `Une expérience de pensée que j'aime beaucoup consiste à vous imaginer sur votre lit de mort. Vous êtes à la porte de la mort et vous savez que la fin est proche. Ensuite, comme par magie, vous êtes transporté dans le temps jusqu'à ce moment précis. Que faites-vous différemment? Quelles choses sont vraiment importantes pour vous? Comment vivriez-vous votre vie si vous aviez une seconde chance?`
                    ]
                },
                {
                    title: "Je suis obsessionnelle",
                    items: [
                        `Mon cerveau n'a qu'un thread. Lorsque je me fixe sur une tâche, vous pouvez à peu près garantir que cela se produira, que cela prenne une heure, une semaine ou un an. Je n'aime pas laisser les choses à moitié faites, cela me donne de l'anxiété.`,
                        `Pour cette raison, j'ai tendance à suivre l'axiome de <a href="https://sive.rs/hellyeah"> "Bon sang ouais ou non" </a>. Je suis très pointilleux sur les projets que je prends et ceux que je transmets, ce qui signifie que je dois souvent dire non à des choses que j'aimerais beaucoup faire. Ça craint, mais c'est la vie. Cela signifie également, cependant, que les projets que je décide d'entreprendre ont tendance à être amusants, intellectuellement stimulants et gratifiants (parfois financièrement, parfois non).`,
                    ]
                },
                {
                    title: "Je veux être nomade numérique",
                    items: [
                        `Le plus heureux que j'ai jamais étée, c'est quand je travaillais à distance à plein temps et que je pouvais voyager où et quand je le voulais. La pandémie a malheureusement limité ma capacité à voyager et j'ai la sensation d'être comme une mouche dans un bocal. C'est horrible de perdre du temps assis à la maison qui pourrait être passé à voir de nouveaux endroits et à rencontrer de nouvelles personnes.`,
                        `J'ai choisi de travailler dans la technologie en raison de son indépendance géographique. Ma vie idéale serait de passer de 6 mois à un an au même endroit, puis de tout laisser derrière moi et d'aller dans un endroit complètement différent. Assez longtemps pour établir des habitudes, se faire des amis, devenir un habitué des cafés et des bars et apprendre à connaître un endroit en profondeur.`,
                        `L'une de mes grandes passions dans la vie est l'apprentissage des langues, et une partie de ce qui alimente mon envie de voyager est l'envie d'être entourée de mots et de sons inconnus.`
                    ]
                }
            ],
            otherThings: {
                title: "D'autres choses sur moi",
                items: [
                    `<b>Je suis une introvertie extravertie.</b> J'aime être avec les gens et j'aime être le centre d'attention quand c'est à mes conditions, mais j'ai besoin de temps seule pour fonctionner.`,
                    `<b>Je suis lente à la colère.</b> Je n'aime pas être bouleversé, c'est épuisant.`,
                    `<b>Je ne m'entends pas très bien avec les conneries.</b> Je tiens à m'entourer de personnes sincères. Je ne supporte pas les gens qui diffusent des airs d'essayer de vous impressionner ou de vous faire aimer. Fumer le cul de quelqu'un d'autre, n'est-ce pas?`,
                    `<b>Je suis très intentionnelle sur les choses que j'autorise dans ma vie.</b> Cela comprend tout: les films, les livres, la musique, les objets, les lieux et les personnes. Tout. Cela me fait paraître snob à certaines personnes. Je m'en fiche. Je fais beaucoup d'efforts pour rechercher et consommer les meilleures choses que je peux. Chaque film que je regarde et chaque album que j'écoute est soigneusement organisé.`,
                    `<b>Je suis vulgaire.</b> Je sacre beaucoup, je fais des blagues sur les bites et j'adore parler de merde de bonne humeur.`
                ]
            }
        }
    }
}