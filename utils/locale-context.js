import React, { useEffect, useState } from "react";
import locales from "./i18n";

var LocaleContext = React.createContext();

function LocaleProvider ({ children }) {
    var [ locale, setLocale ] = useState(locales["en"]);

    useEffect(function getLangFromLocalStorage () {
        if (localStorage.getItem("lang")) {
            var lang = localStorage.getItem("lang");
            setLocale(locales[lang]);
        } else {
            localStorage.setItem("lang", "en");
        }
    }, [])

    return (
        <LocaleContext.Provider value={ { locale, updateLocale } }>
            { children }
        </LocaleContext.Provider>
    );

    function updateLocale (locale) {
        setLocale(locales[locale]);
        localStorage.setItem("lang", locale);
    }
}

function useLocaleContext () {
    var context = React.useContext(LocaleContext);
    if (context === undefined) {
        throw new Error("useLocaleContext must be used within a LocaleProvider");
    }
    return context;
}

export { LocaleProvider, useLocaleContext }