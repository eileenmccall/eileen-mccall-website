var space = process.env.CONTENTFUL_SPACE_ID;
var accessToken = process.env.CONTENTFUL_ACCESS_TOKEN;

var client = require('contentful').createClient({ space, accessToken });

export async function fetchBlogPostIds () {
    var entries = await client.getEntries({ content_type: "blogPost" });
    return entries.items.map(entry => entry.sys.id);
}

export async function fetchBlogPosts () {
    var entries = await client.getEntries({ content_type: "blogPost" });

    return entries.items 
        ? entries.items.map(mapResponse)
        : console.error(`Error getting blog posts.`);
}

export async function fetchBlogPostById (id) {
    var entry = await client.getEntry(id);
    return entry
        ? mapResponse(entry)
        : null;
}

export async function fetchPraise () {
    var entries = await client.getEntries({ content_type: "praise" });
    return entries.items.map(mapResponse);
}

export async function fetchProjects () {
    var entries = await client.getEntries({ content_type: "project" });
    return entries.items.map(mapResponse).map(getImageMapper("thumbnail"));
}

export async function fetchProjectById (id) {
    var entry = await client.getEntry(id);
    return entry
        ? getImageMapper("bannerImage")(mapResponse(entry))
        : null;
}

function mapResponse (
    {
        fields, 
        sys: { 
            id,
            createdAt,
            updatedAt
        } 
    }
) {
    return { id, updatedAt, createdAt, ...fields };
}

function getImageMapper (key) {
    return function mapper (entry) {
        return {
            ...entry,
            [key]: entry[key]
                ? entry[key].fields.file.url
                : null
        };
    }
}