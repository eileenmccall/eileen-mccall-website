import styled from '@emotion/styled'
import React from 'react'

export default function PageSubtitle ({ children, className }) {
    return (
        <Subtitle className={className}>
            { children }
        </Subtitle>
    )
}

var Subtitle = styled.h2`
    font-family: "Montserrat", sans-serif;
    font-size: 1em;
`;