import styled from '@emotion/styled';
import React from 'react'

export default function PageTitle ({ children }) {
    return (
        <Title>
            { children }
        </Title>
    )
}

var Title = styled.h1`
    font-size: 4.6em;
    line-height: .9em;
    margin: 0;
`;