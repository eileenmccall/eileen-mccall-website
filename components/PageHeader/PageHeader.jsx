import styled from '@emotion/styled';
import React from 'react';
import { breakpoints } from '../../utils/breakpoints';
import PageSubtitle from './PageSubtitle';
import PageTitle from './PageTitle';

export default function PageHeader ({ title, subtitle }) {
    return (
        <Container>
            <PageTitle>{ title }</PageTitle>
            <PageSubtitle>{ subtitle }</PageSubtitle>
        </Container>
    );
}

var Container = styled.section`
    padding: 15px;
    ${breakpoints.tablet} {
        max-width: 500px;
        margin: 0 auto;
        padding-top: 50px;
        display: flex;
        flex-direction: column;
        justify-content: center;
    }
`;