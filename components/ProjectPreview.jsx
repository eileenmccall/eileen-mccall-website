import styled from '@emotion/styled';
import React from 'react'
import Tag from './Tag';
import { useRouter } from "next/router";
import Image from "next/image";

export default function ProjectPreview ({ project, tagClicked = () => {} }) {
    var { id, title, description, tags, thumbnail } = project;
    var router = useRouter();

    return (
        <Root>
            <div onClick={navigateToProject} style={{ cursor: "pointer", flexGrow: 1 }}>
                {
                    thumbnail
                    ? (<img src={`http:${thumbnail}`} style={{ width: "100%" }} />)
                    : null
                }
                <CardBody >
                    <ProjectTitle onClick={navigateToProject}>{ title }</ProjectTitle>
                    <p>{ description }</p>
                </CardBody>
            </div>
            <CardFooter>
                {
                    tags.map(function (tag) {
                        return <Tag key={tag} type={tag} tagClicked={ tagClicked }>{tag}</Tag>
                    })
                }
            </CardFooter>
        </Root>
    );

    function navigateToProject () {
        router.push(`projects/${id}`);
    }
}

var Root = styled.article`
    border-radius: 7px;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,.25);
    height: 100%;
    display: flex;
    flex-direction: column;
    transition: all 0.25s ease;
    overflow: hidden;

    &:hover {
        box-shadow: 0 16px 20px rgba(0,0,0,.25);
        transform: scale(1.02);
    }
`;

var CardBody = styled.div`
    padding: 20px;
`;

var CardFooter = styled.footer`
    padding: 20px;
`;

var ProjectTitle = styled.h2`
    margin-top: 0;
    margin-bottom: 10px;
`;