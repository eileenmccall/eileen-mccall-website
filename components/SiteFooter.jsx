import styled from '@emotion/styled'
import React from 'react'

export default function SiteFooter () {
    return (
        <Footer>
            &copy; 2021 Eileen McCall
        </Footer>
    )
}

var Footer = styled.footer`
    background-color: #393939;
    color: white;
    display: flex;
    height: 55px;
    align-items: center;
    justify-content: center;
`;