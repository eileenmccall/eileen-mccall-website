import styled from '@emotion/styled';
import React from 'react'
import colors from '../utils/colors';

export default function Quote({ body, source, sourceDescription }) {
    return (
        <Citation>
            { body }
            <Attribution>&mdash;&thinsp;{ source }{ sourceDescription ? (<><br/>({ sourceDescription })</>) : null }</Attribution>
        </Citation>
    )
}

var Citation = styled.figure`
    font-family: "Playfair Display", serif;
    line-height: 1.7em;
    margin: 0;
    color: ${ colors.dark };
`;

var Attribution = styled.figcaption`
    font-family: "Montserrat", sans-serif;
    text-align: right;
`;