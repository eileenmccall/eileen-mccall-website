import styled from '@emotion/styled'
import React from 'react'
import Quote from "./Quote";
import { breakpoints } from "../utils/breakpoints";
import { useLocaleContext } from '../utils/locale-context';

export default function Praise ({ praise = [] }) {
    var { locale } = useLocaleContext();

    return (
        <Section>
            <Container>
                <SectionTitle>{ locale.praiseTitle }</SectionTitle>
                <PraiseList>
                    {
                        praise.map(function (quote) {
                            var [ body, description ] = locale.lang === "fr"
                                ? [ quote.frenchBody, quote.frenchSourceDescription ]
                                : [ quote.body, quote.sourceDescription ];

                            return <Col key={quote.id}><Quote  body={ body } source={quote.source} sourceDescription={ description }/></Col>
                        })
                    }
                </PraiseList>
            </Container>
        </Section>
    );
}

var Section = styled.section`
    padding: 100px 25px;
    background-color: #EBC924;
    font-size: 1rem;
`;

var Container = styled.div`
    margin: 0 auto;
    max-width: 1200px;
`;

var SectionTitle = styled.h3`
    font-size: 2em;
    text-align: right;
    max-width: 300px;
    margin-left: auto;
    margin-top: 0;
`;

var PraiseList = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

var Col = styled.div`
    flex-basis: 100%;
    padding: 25px 15px;

    ${breakpoints.tablet} {
        flex-basis: 50%;
    }

    ${breakpoints.laptop} {
        flex-basis: 33%;
    }

    ${breakpoints.laptopL} {
        flex-basis: 25%;
    }
`;

