import styled from '@emotion/styled';
import React from 'react'
import colors from '../../utils/colors';

export default function ProjectDetailsTitle ({ title }) {
    return (
        <Root>{ title }</Root>
    )
}


var Root = styled.h1`
    font-size: 3em;
    margin: -1px 0;
    color: white;
    background-color: ${ colors.dark };
    padding: 0 10px;
`;