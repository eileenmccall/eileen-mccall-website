import ProjectDetailsBanner from "./ProjectDetailsBanner";
import ProjectDetailsContent from "./ProjectDetailsContent";
import ProjectDetailsOverlay from "./ProjectDetailsOverlay";
import ProjectDetailsSubtitle from "./ProjectDetailsSubtitle";
import ProjectDetailsTagList from "./ProjectDetailsTagList";
import ProjectDetailsTitle from "./ProjectDetailsTitle";

export var ProjectDetails = {
    Subtitle: ProjectDetailsSubtitle,
    TagList: ProjectDetailsTagList,
    Title: ProjectDetailsTitle,
    Overlay: ProjectDetailsOverlay,
    Banner: ProjectDetailsBanner,
    Content: ProjectDetailsContent
}