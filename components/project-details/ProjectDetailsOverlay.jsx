import React from 'react';
import styled from '@emotion/styled';

export default function ProjectDetailsOverlay ({ children }) {
    return (
        <Root>
            { children }
        </Root>
    );
}

var Root = styled.div`
    background-color: rgba(0,0,0,0.2);
    color: white;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
`;