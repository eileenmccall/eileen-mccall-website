import styled from '@emotion/styled';
import React from 'react'
import colors from '../../utils/colors';
import { useLocaleContext } from '../../utils/locale-context';

export default function ProjectDetailsSubtitle({ date, wordCount = 0 }) {
    var { 
        locale: { 
            lang,
            projectDetails: {
                timeToReadBefore,
                timeToReadAfter
            }
        } 
    } = useLocaleContext();
    const WORDS_PER_MINUTE = 250;

    var dateLocale = lang === "en" ? "en-US" : "fr-FR";
    var formattedDate = new Date(date)
        .toLocaleDateString(
            dateLocale, 
            { 
                year: 'numeric', 
                month: 'long', 
                day: 'numeric' 
            }
        );

    var timeToRead = Math.ceil(wordCount / WORDS_PER_MINUTE);

    return (
        <Root>{ formattedDate } - {timeToReadBefore} { timeToRead } {timeToReadAfter}</Root>
    )
}


var Root = styled.p`
    margin: 0;
    background-color: ${ colors.dark };
    padding: 5px 10px;
`;