import styled from '@emotion/styled';
import React from 'react'

export default function ProjectDetailsContent ({ html }) {
    return (
        <Root dangerouslySetInnerHTML={{ __html: html }} />
    );
}

var Root = styled.div`
    padding: 25px 0;
    margin: 0 auto;
`;