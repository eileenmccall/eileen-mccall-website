import styled from '@emotion/styled';
import React from 'react'

export default function ProjectDetailsBanner ({ image, children}) {
    return (
        <Root image={image}>
            { children }
        </Root>
    );
}

var Root = styled.div`
    height: 500px;
    background-position: cover;
    background-image: url(${ props => `https://${ props.image }`});
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
`;