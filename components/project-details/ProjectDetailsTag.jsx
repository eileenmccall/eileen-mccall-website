import styled from '@emotion/styled';
import React from 'react';
import colors from '../../utils/colors';
import { useRouter } from "next/router";

export default function ProjectDetailsTag ({ type, children }) {
    var router = useRouter();
    return (
        <Root type={type} onClick={ handleClick }>
            { children }
        </Root>
    );

    function handleClick () {
        router.push({
            pathname: "/projects",
            query: {
                filter: type
            }
        });
    }
}

var Root = styled.a`
    color: ${ ({type}) => colors[type] || "white" };
    font-family: "Playfair Display", serif;
    padding: 0 3px;
    text-decoration: none;
    
    &:hover {
        cursor: pointer;
    }
`;