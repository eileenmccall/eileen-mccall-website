import styled from '@emotion/styled';
import React from 'react'
import colors from '../../utils/colors';
import ProjectDetailsTag from './ProjectDetailsTag';

export default function ProjectDetailsTagList ({ tags = [] }) {
    return (
        <Root>
            {
                tags.map(function (tag) {
                    return (
                        <ProjectDetailsTag key={tag} type={ tag }>#{ tag }</ProjectDetailsTag>
                    );
                })
            }
        </Root>
    )
}

var Root = styled.div`
    padding: 10px;
    background-color: ${ colors.dark };
`;