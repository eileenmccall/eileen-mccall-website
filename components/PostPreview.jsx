import styled from '@emotion/styled'
import Link from 'next/link';
import React from 'react'

var Background = styled.div`
    background-color: white;
    border: 1px solid lightgrey;
    padding: 15px;
    -webkit-box-shadow: 3px 3px 5px 0px rgba(227,227,227,1);
    -moz-box-shadow: 3px 3px 5px 0px rgba(227,227,227,1);
    box-shadow: 3px 3px 5px 0px rgba(227,227,227,1);
`;

var PreviewHeader = styled.h2`
    cursor: pointer;
`;

export default function PostPreview({ post }) {
    var { id, title } = post;

    return (
        <Background>
            <Link href={`/posts/${ id }`}><PreviewHeader>{ title }</PreviewHeader></Link>
            Here's a post!
        </Background>
    )
}
