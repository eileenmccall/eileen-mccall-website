import React from 'react'
import styled from '@emotion/styled';


var Grid = styled.section`
    line-height: 0;
    
    -webkit-column-count: 3;
    -webkit-column-gap:   0px;
    -moz-column-count:    3;
    -moz-column-gap:      0px;
    column-count:         3;
    column-gap:           0px;

    img {
        width: 100% !important;
        height: auto !important;
    }

    @media (max-width: 1200px) {
        -moz-column-count:    4;
        -webkit-column-count: 4;
        column-count:         4;
    }

    @media (max-width: 1000px) {
        -moz-column-count:    3;
        -webkit-column-count: 3;
        column-count:         3;
    }
    
    @media (max-width: 800px) {
        -moz-column-count:    2;
        -webkit-column-count: 2;
        column-count:         2;
    }

    @media (max-width: 400px) {
        -moz-column-count:    1;
        -webkit-column-count: 1;
        column-count:         1;
    }
`;

export default function PossumGrid() {
    return (
        <Grid>
            <img src="https://www.adirondackalmanack.com/wp-content/uploads/2020/02/Possums-courtesy-US-Fish-and-Wildlife.png" />
            <img src="https://www.sharetheoutdoors.com/wp-content/uploads/2017/01/For-STO-01172017-HUNTING-Picture-1of1-Possums-Part-1of2-Jill-Easton.jpg" />
            <img src="https://avltoday.6amcity.com/wp-content/uploads/sites/7/2020/09/possums-north-carolina-official-marsupial-avltoday.jpg" />
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Opossum_2.jpg/1200px-Opossum_2.jpg" />
            <img src="https://animalcontrolspecialists.com/wp-content/uploads/2018/07/AdobeStock_62861269-1080x675.jpeg" />
            <img src="https://ei.marketwatch.com/Multimedia/2019/12/31/Photos/ZQ/MW-HX580_opossu_20191231103449_ZQ.jpg?uuid=14df5e6c-2be3-11ea-b280-9c8e992d421e" />
            <img src="https://res.cloudinary.com/sagacity/image/upload/c_crop,h_667,w_1000,x_0,y_0/c_limit,f_auto,fl_lossy,q_80,w_1080/shutterstock_442647010_brbfkm.jpg" />
            <img src="https://images2.minutemediacdn.com/image/upload/c_crop,h_1193,w_2120,x_0,y_95/v1554700180/shape/mentalfloss/539787-istock-164188559.jpg?itok=D4MWCq8h" />
            <img src="https://www.dictionary.com/e/wp-content/uploads/2011/01/opossum-500x310.jpg" />
            <img src="https://www.gazettenet.com/getattachment/ada248d8-617e-4e95-9c56-92a987303dbc/c7-14out-EarthMatters-hg-071418-ph2" />
            <img src="https://blogs.loc.gov/folklife/files/2019/08/Opossum-by-Paul-Hurtado.jpg" />
            <img src="https://irp-cdn.multiscreensite.com/e2b5886a/dms3rep/multi/possumvsopu.png" />

            <img src="https://www.presstelegram.com/wp-content/uploads/2018/03/possum.jpg?w=1024&h=773" />

        </Grid>
    )
}
