import styled from '@emotion/styled';
import React, { useEffect, useState } from 'react';
import Link from 'next/link'
import { keyframes } from '@emotion/react';
import { useLocaleContext } from '../utils/locale-context';
import colors from '../utils/colors';

export default function SiteHeader ({ theme = "default", onShowMenu = () => {} }) {

    var { locale, updateLocale } = useLocaleContext();

    // var SUBTITLE_LIST = [ 
    //     "Web Developer",
    //     "JavaScript Geek",
    //     "Coffee Addict",
    //     "Mothman Enthusiast",
    //     "Kung Fu Movie Scholar",
    //     "Francophile",
    //     "Doing her best"
    // ];

    // var [subtitleIndex, setSubtitleIndex ] = useState(0);

    // useEffect(function iterateSubtitle () {
    //     var interval = setInterval(function () {
    //         setSubtitleIndex(c => c < SUBTITLE_LIST.length - 1 ? c + 1 : 0);
    //     }, 5000);

    //     return function removeInterval () {
    //         clearInterval(interval);
    //     };
    // }, [subtitleIndex]);

    return (
        <StyledHeader>
            <MenuButton theme={theme} onClick={onShowMenu}>Menu</MenuButton>
            
            {/* <div>
                <SiteTitle>
                    Eileen McCall
                </SiteTitle>
                <SiteSubtitle>WEB DEVELOPER</SiteSubtitle>
            </div> */}

            <div>
                <LanguageSelector
                    theme={theme}
                    type="button" 
                    name="english"
                    active={ locale.lang === "en"}
                    onClick={() => updateLocale("en")}
                >
                    EN.
                </LanguageSelector>
                <LanguageSelector 
                    theme={theme}
                    type="button" 
                    name="english"
                    active={ locale.lang === "fr" }
                    onClick={() => updateLocale("fr")}
                >
                    FR.
                </LanguageSelector>
                </div>
        </StyledHeader>
    )
}

var StyledHeader = styled.header`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    z-index: 1;
    justify-content: space-between;
    align-items: center;
    display: flex;
    padding: 15px;
    height: 55px;
`;

var MenuButton = styled.button`
    color: ${ props => props.theme === "default" ? `${ colors.dark }`: props.theme };
    font-family: "Playfair Display", serif;
    font-size: 16px;
    font-weight: bold;
    border: none;
    background: transparent;
    text-decoration: none;
    border-radius: 0;
    cursor: pointer;
`;

var SiteTitle = styled.h1`
    margin: 0;
    text-align: center;
`;

var SiteSubtitle = styled.h4`
    text-align: center;
    font-family: 'Montserrat', sans-serif;
    font-weight: 300;
    margin: 0;
`;

var LanguageSelector = styled.button`
    background: transparent;
    border: none;
    color: ${function ({ active, theme }) {
        if (active) {
            if (theme === "default") {
                return colors.dark;
            }

            return theme;
        }

        return "#bcbcbc";
    }};
    cursor: pointer;
    display: inline-block;
    font-family: "Playfair Display", serif;
    font-size: 16px;
    font-weight: bold;
    text-decoration: none;
`;

// var fadeInOut = keyframes`
//     from, to {
//         opacity: 0;
//     }

//     25%, 75% {
//         opacity: 1;
//     }
// `;

// var SiteSubtitle = styled.h2`
//     font-style: italic;
//     font-weight: 500;
//     color: white;
//     margin: 0 0 5px 0;

//     animation: ${ fadeInOut } 5s ease infinite;
// `;

var StyledA = styled.a`
    color: white;
    cursor: pointer;
    margin: 0 10px;
`;