import styled from '@emotion/styled';
import React from 'react'

import Twitter from './icons/Twitter';
import Gitlab from "./icons/Gitlab";
import Linkedin from "./icons/Linkedin";
import Instagram from './icons/Instagram';

export default function Socials () {
    return (
        <div>
            <SocialLink href="https://www.twitter.com/eileengmccall">
                <Twitter role="link" height="18px" width="18px"/>
            </SocialLink>

            <SocialLink href="https://www.gitlab.com/eileenmccall">
                <Gitlab role="link" height="18px" width="18px"/>
            </SocialLink>

            <SocialLink href="https://www.linkedin.com/in/eileenmccall/">
                <Linkedin role="link" height="18px" width="18px"/>
            </SocialLink>

            <SocialLink href="https://www.instagram.com/eileengmccall">
                <Instagram role="link" height="18px" width="18px"/>
            </SocialLink>
        </div>
    );
}

var SocialLink = styled.a`
    cursor: pointer;
    display: inline-block;
    margin: 0 10px;
    /* 

    &:first-of-type {
        margin-left: 5px;
    }

    &::last-of-type {
        margin-right: 0;
    } */
`;
