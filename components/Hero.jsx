import styled from '@emotion/styled';
import React from 'react'
import { breakpoints } from "../utils/breakpoints";
import { useLocaleContext } from '../utils/locale-context';
import Socials from './Socials';

export default function Hero () {
    var { locale } = useLocaleContext();

    return (
        <Container>
            <div style={{ display: "flex", alignItems: "center", justifyContent: "center", marginBottom: "30px" }}>
                <p style={{ fontSize: "150%", margin: "0" }}>⚠️</p> <p style={{ textAlign: "center", margin: "0 15px", lineHeight: "150%" }}>{ locale.underConstructionText }</p> <p style={{ fontSize: "150%", margin: "0" }}>⚠️</p>
            </div>

            <Name>Eileen McCall</Name>
            <Tagline>{ locale.tagline.toUpperCase() }</Tagline>
            <Socials />
            
            <Intro>
                { locale.introText }
                <br /><br />
                { locale.introQuip }
            </Intro>
        </Container>
    );
}

var Container = styled.section`
    padding: 15px;
    ${breakpoints.tablet} {
        max-width: 500px;
        margin: 0 auto;
        height: calc(100vh - 175px);
        display: flex;
        flex-direction: column;
        justify-content: center;
    }


    /* height: calc(100vh - 200px);
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: stretch;
    max-width: 500px;
    padding: 15px;
    margin: 0 auto;
    font-size: 1rem; */
`;

var Name = styled.h1`
    font-size: 4em;
    line-height: .9em;
    margin: 0;
    /* font-size: 75px;
    line-height: 75px;
    margin: 0; */
`;

var Tagline = styled.h2`
    font-family: "Montserrat", sans-serif;
    margin: 12px 0;

    /* font-size: 2em;
    margin-bottom: 5px; */
`;

var Intro = styled.p`
    line-height: 1.5em;
    margin-top: 28px;
    /* line-height: 28px; */
`;


