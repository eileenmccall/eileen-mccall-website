import React from 'react'
import styled from '@emotion/styled';
import colors from '../utils/colors';

var TagStyles = styled.a`
    color: ${ ({type}) => colors[type] || colors.dark };
    font-family: "Playfair Display", serif;
    padding: 0 3px;
    text-decoration: none;
    
    &:hover {
        cursor: pointer;
    }
`;

export default function Tag ({ type, children, tagClicked = () => {} }) {
    return (
        <TagStyles type={type} onClick={() => tagClicked(type)}>#{ children }</TagStyles>
    );
}
