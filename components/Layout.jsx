import React, { useState } from 'react'
import Menu from './Menu';
import SiteHeader from './SiteHeader'
import { useRouter } from 'next/router'
import styled from '@emotion/styled';

export default function Layout({ theme = "default", children }) {
    var [ showMenu, setShowMenu ] = useState(false);
    var router = useRouter();

    return (
        <SiteContainer menuIsOpen={showMenu}>
            { showMenu ? (<Menu onHide={() => setShowMenu(false)} onNav={navigationHandler}/>) : null }
            <SiteHeader theme={theme} onShowMenu={() => setShowMenu(true)}/>
            { children }
        </SiteContainer>
    );

    function navigationHandler (href) {
        router.prefetch(href).then(function () {
            setShowMenu(false);
            router.push(href);
        });
    }
}

var SiteContainer = styled.div(({ menuIsOpen }) => ({
    overflow: menuIsOpen ? "hidden" : "visible",
    height: menuIsOpen ? "100vh" : "intiial",
    width: menuIsOpen ? "100vw" : "initial"
}));