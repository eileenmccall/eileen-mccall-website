import styled from '@emotion/styled'
import React, { useLayoutEffect, useRef, useState } from 'react'
import { breakpoints } from '../utils/breakpoints';
import { useLocaleContext } from "../utils/locale-context";

export default function Menu ({ onNav = () => {}, onHide = () => {} }) {
    var [navHeight, setNavHeight] = useState(0);
    var [navWidth, setNavWidth] = useState(0);
    var navRef = useRef(null);
    var { locale } = useLocaleContext();

    useLayoutEffect(function () {
        setNavHeight(navRef.current.clientHeight);
        setNavWidth(navRef.current.clientWidth);
    });

    return (
        <MenuDiv>
            <CloseButton onClick={onHide}>{ locale.menuClose }</CloseButton>
            <MenuNav ref={navRef} navHeight={navHeight} navWidth={navWidth}>
                <NavLink onClick={() => onNav("/")}>{ locale.menuHome }</NavLink>
                <NavLink onClick={() => onNav("/projects")}>{ locale.menuWork }</NavLink>
                <NavLink onClick={() => onNav("/about")}>{ locale.menuAbout }</NavLink>
                <NavLink onClick={() => onNav("/contact")}>{ locale.menuContact }</NavLink>
                {/* <NavLink onClick={() => onNav("/possums")}>{ locale.menuPossums }</NavLink> */}
            </MenuNav>
        </MenuDiv>
    );
}

var MenuDiv = styled.div`
    position: sticky;
    height: 100vh;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #393939;
    z-index: 69;
`;

var CloseButton = styled.button`
    color: white;
    position: absolute;
    top: 15px;
    left: 15px;
    font-family: "Playfair Display", serif;
    border: none;
    background: transparent;
`;

var MenuNav = styled.nav`
    position: relative;
    display: inline-block;
    top: ${props => `calc(45% - ${Math.floor(props.navHeight / 2)}px)`};
    left: ${props => `calc(50% - ${Math.floor(props.navWidth / 2)}px)`};
`;

var NavLink = styled.a`
    color: white;
    display: block;
    font-family: "Playfair Display", serif;
    font-size: 2em;
    cursor: pointer;
    text-decoration: none;

    &:hover {
        text-decoration: underline;
    }

    ${breakpoints.tablet} {
        font-size: 3em;
    }

    ${breakpoints.laptop} {
        font-size: 4em;
    }
`;
