import React, { useRef } from 'react'
import { fetchBlogPostById, fetchBlogPostIds } from '../../utils/contentful';
import Layout from "../../components/Layout";
import remark, { parse } from "remark";
import html from "remark-html";
import Link from 'next/link';

export default function BlogPost ({ post }) {
    var { title, body } = post;

    var { current: bodyHTML } = useRef(
        remark()
            .use(parse)
            .use(html)
            .processSync(body)
    );

    return (
        <Layout>
            <Link href="/posts">Back to posts</Link>
            <h1>{ title }</h1>
            <div dangerouslySetInnerHTML={{ __html: bodyHTML}}></div>
        </Layout>
    )
}

export async function getStaticPaths () {
    var ids = await fetchBlogPostIds();

    var paths = ids.map(id => ({ params: { id } }));

    return { paths, fallback: false };
}

export async function getStaticProps ({ params: { id }}) {
    // var caseFile = await dataQueries.fetchCaseFileById(id);
    var post = await fetchBlogPostById(id);

    return {
        props: {
            post
        }
    };
}