import styled from '@emotion/styled';
import React from 'react'
import Layout from '../../components/Layout'
import PageTitle from '../../components/PageHeader/PageTitle';
import PostPreview from '../../components/PostPreview';
import { fetchBlogPosts } from '../../utils/contentful';

var PostList = styled.div`
    display: flex;
    flex-direction: column;
    padding: 15px 0;

    & > * {
        margin-bottom: 15px;
    }
`;

export default function Posts ({ posts = [] }) {

    return (
        <Layout>
            <PageTitle>Work</PageTitle>
            <PostList>
                { 
                    posts.map(function (post) {
                        return <PostPreview key={post.id} post={post}/>
                    })
                }
            </PostList>
        </Layout>
    );
}

export async function getStaticProps() {
    const posts = await fetchBlogPosts();

    return {
        props: {
            posts,
        },
    };
}