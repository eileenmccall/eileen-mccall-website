import styled from '@emotion/styled';
import React from 'react'
import Layout from '../components/Layout'
import PageHeader from '../components/PageHeader/PageHeader'
import { useLocaleContext } from '../utils/locale-context';

export default function About () {
    var { locale: { about } } = useLocaleContext();

    return (
        <Layout>
            <PageHeader title={ about.title } subtitle={ about.subtitle }/>
            <Container>                
                <Timeline>
                    <TimelineTitle>{ about.timelineTitle }</TimelineTitle>
                    {
                        about.timeline.map(function (x) {
                            return (
                                <TimelineItem key={ x.year }>
                                    <TimelineHeader>{ x.year }</TimelineHeader>
                                    <TimelineBody>
                                    { 
                                        x.items.map(function (item) {
                                            return (
                                                <p key={item} dangerouslySetInnerHTML={{ __html: item }} />
                                            );
                                        })
                                    } 
                                    </TimelineBody>
                                </TimelineItem>
                            );
                        })
                    }
                </Timeline>

                {
                    about.sections.map(function (section) {
                        return (
                            <>
                                <SectionTitle>{ section.title }</SectionTitle>
                                <hr />
                                {
                                    section.items.map(function (item) {
                                        return (
                                            <p key="item" dangerouslySetInnerHTML={{ __html: item }} />
                                        )
                                    })
                                }
                            </>
                        )
                    })
                }

                <SectionTitle>{ about.otherThings.title }</SectionTitle>
                <hr/>
                <OtherThings>
                    {
                        about.otherThings.items.map(function (item) {
                            return (
                                <li key="item" dangerouslySetInnerHTML={{ __html: item }} />
                            )
                        })
                    }
                </OtherThings>
            </Container>
        </Layout>
    );
}

var Container = styled.div`
    max-width: 900px;
    margin: 0 auto;
    padding: 15px;
`;

var Timeline = styled.ul`
    list-style: none;
    padding-left: 0;
    background-color: #f7f7f7;
    padding: 25px;
    margin-top: 50px;
    margin-bottom: 50px;
`;

var TimelineItem = styled.li`
    display: flex;
`;

var TimelineHeader = styled.h3`
    flex-basis: 125px;
    flex-shrink: 0;
    font-family: Montserrat, sans-serif;
    font-weight: bold;
    text-align: right;
    padding-right: 25px;
`;

var TimelineTitle = styled.h2`
    text-align: center;
    font-size: 1.3em;
`;

var TimelineBody = styled.div``;

var SectionTitle = styled.h2`
    font-size: 1.3em;
    margin-top: 50px;
`;

var OtherThings = styled.ul`
    margin-bottom: 50px;
    
    li {
        margin-bottom: 15px;
    }
`;