import Head from 'next/head'
import Hero from '../components/Hero'
import Layout from '../components/Layout'
import Menu from '../components/Menu'
import Praise from '../components/Praise'
import SiteFooter from '../components/SiteFooter'
import SiteHeader from '../components/SiteHeader'
import styles from '../styles/Home.module.css'
import { fetchPraise } from '../utils/contentful'

export default function Home ({ praise }) {

    return (
        <Layout>
            <Hero />
            <Praise praise={praise}/>
        </Layout>
        // <Layout>
        //     <h1>Hey.</h1>
        //     <p>I'm still building this website.</p>
        //     <p>Come back later, there might be more stuff.</p>
        //     <p>You look great today, by the way.</p>

        //     <h2>Quick Links:</h2>
        //     <ul>
        //         <li><a href="https://docs.google.com/document/d/1NqgX1IrV5CNoP4y85rlnKzUEFryPHXdvpJoRleC7yoQ/edit?usp=sharing">Resume</a></li>
        //         <li><a href="https://www.linkedin.com/in/eileenmccall">LinkedIn</a></li>
        //         <li>GitLab</li>
        //     </ul>

        //     <h2>A little bit about me</h2>
        //     <hr />
        //     <p>Like everybody, I detest writing about myself, so I'll keep this brief.</p>
        //     <p>My name is Eileen, I'm a web developer and recovering small-town Iowan. My journey in web development started in 2016 when I realized that my creative writing degree wasn't exactly producing the Scrooge McDuck levels of wealth and opulence I thought it would when I was 18. During nights and weekends I began teaching myself to code using what online resources I could find, and I eventually landed an entry-level frontend development job.</p>
        //     <p>My gracious employers, We Write Code (a software consultancy based out of Des Moines), served as both mentors and friends as I honed my craft in a trial by fire. I came out the other end a relatively competant developer, having made invaluable connections with the DSM startup community and learned inumerable painful lessons about software development and life in general. I consider those years some of the best of my life, and I remain close to the WWC team to this day.</p>
        //     <p>Having moved on from WWC, I spent much of the intervening time expanding my knowledge of all-things web. I read books and articles, attended usergroup meetings, and did online courses (most or all of which can be found on my "recommendations" page). The depth of frontend knowledge I developed during that time proved to be an invaluable asset, and helped me gain my current position as a software engineer at DHI Group, Inc, where I am known to be one of the most highly knowledgeable devs at the company.</p>
        //     <p>These days, when I'm not working, I still spend a lot of my time consuming all the books, tutorials, and courses I can get my hands on, and building projects which put that information to use. Self-instruction has become my #1 passion in life, and it has served me well. I also enjoy video games; coffee, whiskey, and craft beer (wow so unique); playing the guitar; and watching Godzilla movies. Ask me to bore you with Godzilla trivia sometime.</p>

        //     <h2>Projects</h2>
        //     <hr />
        //     <ul>
        //         <li>Pomodoro Timer #vanilla-js</li>
        //         <li>Metronome #vanilla-js</li>
        //         <li>Digital Bartender #react</li>
        //     </ul>

        //     <h2>Talks, Tutorials, Workshops:</h2>
        //     <hr />

        //     <h2>Kind words</h2>
        //     <hr />
        //     <blockquote>
        //         "Whenever we don't understand something, we go to Eileen." - Alex Rodriguez, former coworker
        //   </blockquote>
        //     <blockquote>
        //         "[She] knows [her] shit." - Jessie Puls, Team Lead
        //   </blockquote>
        // </Layout>
    )
}

export async function getStaticProps() {
    const praise = await fetchPraise();

    return {
        props: {
            praise,
        },
    };
}