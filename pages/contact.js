import { css } from '@emotion/react';
import styled from '@emotion/styled';
import React, { useState } from 'react'
import Layout from "../components/Layout";
import PageHeader from "../components/PageHeader/PageHeader";
import Socials from '../components/Socials';
import colors from "../utils/colors";
import { useLocaleContext } from '../utils/locale-context';

export default function Contact () {
    var { locale } = useLocaleContext();
    var [ formState, setFormState ] = useState({
        email: "",
        subject: "",
        message: "",
        botField: ""
    });
    var [ formSubmitted, setFormSubmitted ] = useState(false);
    var [ formError, setFormError ] = useState(null);

    return (
        <Layout>
            <PageHeader title={ locale.contactTitle } subtitle={ locale.contactSubtitle } />
            <Container>
                <p>{ locale.contactSocialMessage }</p>
                <Socials />

                {
                    formError
                        ? (<ErrorMessage>{ locale.contactOnError }</ErrorMessage>)
                        : (null)
                }

                {
                    formSubmitted
                        ? (<SuccessMessage>{ locale.contactOnSuccess }</SuccessMessage>)
                        : (null)
                }

                {
                    !formSubmitted && !formError
                        ? (
                            <form name="contact"  onSubmit={handleSubmit}>
                                <FormGroup>
                                    <InputLabel aria-label="email" htmlFor="emailInput">{ locale.contactEmail }</InputLabel>
                                    <Input aria-labelledby="email" name="email" value={formState.email} onChange={handleChange} id="emailInput" type="email" placeholder={ locale.contactEmailPlaceholder} />
                                </FormGroup>
                        
                                <FormGroup>
                                    <InputLabel aria-label="subject" htmlFor="subjectInput">{ locale.contactSubject }</InputLabel>
                                    <Input aria-labelledby="subject" name="subject" value={formState.subject} onChange={handleChange} id="subjectInput" type="text" placeholder={ locale.contactSubjectPlaceholder} />
                                </FormGroup>
                                
                                <FormGroup>
                                    <InputLabel aria-label="message" htmlFor="bodyInput">{ locale.contactMessage }</InputLabel>
                                    <TextArea aria-labelledby="message" name="message" value={formState.message} onChange={handleChange} id="bodyInput" placeholder={ locale.contactMessagePlaceholder}></TextArea>
                                </FormGroup>

                                <label style={{ position: "absolute", visibility: "hidden" }}>Don't fill this out if you're human! <input type="hidden" name="bot-field" onChange={handleChange}></input></label>

                                <SubmitButton type="submit">Get at me</SubmitButton>
                            </form>     
                        )
                        : (null)
                }
            </Container>
        </Layout>
    );

    function handleChange (e) {
        var { name, value } = e.target;
        setFormState((currentState) => ({
            ...currentState,
            [name]: value
        }));
    }

    function encode (data) {
        return Object.keys(data)
            .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
            .join("&");
    }

    function handleSubmit (e) {
        e.preventDefault();

        fetch("/", {
            method: "POST",
            headers: { "Content-Type": "application/x-www-form-urlencoded" },
            body: encode({ "form-name": "contact", "netlify-honeypot": "botField", ...formState})
        })
            .then(function onFormSubmitSuccess () {
                setFormSubmitted(true);
            })
            .catch(function onFormSubmitError (error) {
                setFormError(error);
            });
    }
}

var Container = styled.div`
    max-width: 500px;
    margin: 0 auto;
`;

var FormGroup = styled.div`
    margin: 15px 0;
`;

var InputLabel = styled.label`
    clip: rect(0 0 0 0);
    clip-path: inset(50%);
    height: 1px;
    overflow: hidden;
    position: absolute;
    white-space: nowrap;
    width: 1px;
`;

var accessiblePlaceholder = css`
    ::-webkit-input-placeholder {
        color: #626262;
    }
    ::-moz-placeholder {
        color: #626262;
    }
    :-moz-placeholder { /* Older versions of Firefox */
        color: #626262;
    }
    :-ms-input-placeholder {
        color: #626262;
    }
`;

var Input = styled.input`
    display: block;
    width: 100%;
    max-width: 500px;
    padding: 7px;
    border-radius: 5px;
    box-shadow: none;
    border: 1px solid gray;
    ${ accessiblePlaceholder };
`;

var TextArea = styled.textarea`
    width: 100%;
    max-width: 500px;
    min-height: 100px;
    padding: 7px;
    border-radius: 5px;
    border: 1px solid gray;
    ${ accessiblePlaceholder };
`;

var SubmitButton = styled.button`
    padding: 13px;
    border: none;
    text-decoration: none;
    box-shadow: none;
    border-radius: 5px;
    background-color: ${ colors.react };
    color: ${ colors.dark };
    cursor: pointer;
`;

var SuccessMessage = styled.p`
    color: #155724;
    background-color: #d4edda;
    border-radius: 5px;
    padding: 25px;
`;

var ErrorMessage = styled.p`
    color: #721c24;
    background-color: #f8d7da;
    border-radius: 5px;
    padding: 25px;
`;