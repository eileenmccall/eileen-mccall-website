import '../styles/index.css'
import { LocaleProvider } from '../utils/locale-context'

function MyApp({ Component, pageProps }) {
    return (
        <LocaleProvider>
            <Component {...pageProps} />
        </LocaleProvider>
    );
}

export default MyApp
