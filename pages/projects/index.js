import styled from '@emotion/styled';
import { withRouter } from 'next/router';
import React, { useEffect, useState } from 'react'
import Layout from "../../components/Layout";
import PageHeader from '../../components/PageHeader/PageHeader';
import ProjectPreview from '../../components/ProjectPreview';
import colors from '../../utils/colors';
import { fetchProjects } from '../../utils/contentful';
import { useLocaleContext } from '../../utils/locale-context';

export default withRouter(function Projects ({ router, projects = [] }) {
    var { locale } = useLocaleContext();

    var [ filteredProjects, setFilteredProjects ] = useState(projects);
    var [ activeFilter, setActiveFilter ] = useState(null);

    useEffect(function updateFilterFromQueryParams () {
        var filter = router.query.filter;
        setActiveFilter(filter || null);
        setFilteredProjects(filter ? projects.filter(project => project.tags.includes(filter)) : projects);
    }, [ router ])

    return (
        <Layout>
            <PageHeader title={locale.workTitle} subtitle={locale.workSubtitle}></PageHeader>

            <Filters>
                <Filter active={activeFilter === null} onClick={() => filterProjects(null)}>{ locale.lang === "en" ? "All" : "Tous" }.</Filter>
                <Filter active={activeFilter === "javascript"} type="javascript" onClick={() => filterProjects("javascript")}>JavaScript.</Filter>
                <Filter active={activeFilter === "node"} type="node" onClick={() => filterProjects("node")}>Node.</Filter>
                <Filter active={activeFilter === "react"} type="react" onClick={() => filterProjects("react")}>React.</Filter>
                <Filter active={activeFilter === "next"} type="next" onClick={() => filterProjects("next")}>Next.</Filter>
            </Filters>

            <ProjectsContainer>
                {
                    filteredProjects.length > 0
                    ? filteredProjects.map(function (project) {
                        return (
                            <ProjectsItem key={ project.id }>
                                <ProjectPreview project={project} tagClicked={filterProjects}/>
                            </ProjectsItem>
                        );
                    })
                    : (<p>There's nothing here... yet!</p>)
                }
            </ProjectsContainer>
        </Layout>
    );

    function filterProjects (tag) {
        setActiveFilter(tag);
        if (tag) {
            router.replace({
                pathname: "/projects",
                query: {
                    filter: tag
                }
            });
            var results = projects.filter(project => project.tags.includes(tag));
            setFilteredProjects(results);
        } else {
            router.replace({
                pathname: "/projects"
            });
            setFilteredProjects(projects);
        }
    }
})

var Filter = styled.a`
    color: ${ props => colors[props.type] || colors.dark };
    font-size: 26px;
    font-family: "Playfair Display";
    cursor: pointer;
    text-decoration: ${ props => props.active ? "underline" : "none" };

    &:hover {
        text-decoration: underline;
    }
`;

var Filters = styled.nav`
    text-align: center;
    margin-bottom: 50px;
    
    & > * {
        margin: 0 5px;
    }

    & > *:first-of-type {
        margin-left: 0;
    }

    & > *:last-of-type {
        margin-right: 0;
    }
`;

var ProjectsContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    align-items: stretch;
    max-width: 1140px;
    margin: 0 auto;
`;

var ProjectsItem = styled.div`
    flex-basis: 100%;
    padding: 0 15px;
    margin-bottom: 30px;

    @media(min-width: 600px) {
        flex-basis: 50%;
    }

    @media(min-width: 900px) {
        flex-basis: 33%;
    }
    
`;

export async function getStaticProps() {
    const projects = await fetchProjects();

    return {
        props: {
            projects,
        },
    };
}