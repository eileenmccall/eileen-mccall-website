import React, { useMemo, useState } from 'react'
import remarkHtml from "remark-html";
import remark, { parse } from "remark";

import { fetchProjectById, fetchProjects } from '../../utils/contentful';
import Layout from "../../components/Layout";
import { useLocaleContext } from '../../utils/locale-context';
import { ProjectDetails } from "../../components/project-details";
import Countable from "countable";
import Link from 'next/link';
import styled from '@emotion/styled';

export default function ProjectDetailsPage ({ project }) {
    console.log(project);

    var { locale: { lang } } = useLocaleContext();

    var title = lang === "en" ? project.title : project.frenchTitle;
    var body = lang === "en" ? project.body : project.frenchBody;
    var [ wordCount, setWordCount ] = useState(0);

    var html = useMemo(function () {
        var result = remark()
            .use(parse)
            .use(remarkHtml)
            .processSync(body);

        Countable.count(result.contents, (count) => {
            setWordCount(count.words)
        }, { stripTags: true });

        return result;
    }, [ lang ]);

    return (
        <Layout theme="white">
            <ProjectDetails.Banner image={project.bannerImage}>
                <ProjectDetails.Overlay>
                    <ProjectDetails.TagList tags={project.tags} />
                    <ProjectDetails.Title title={title} />
                    <ProjectDetails.Subtitle date={ project.updatedAt } wordCount={ wordCount } />
                </ProjectDetails.Overlay>
            </ProjectDetails.Banner>

            <Container>
                <Link href="/projects">Back to Projects</Link>
                <ProjectDetails.Content html={html} />
            </Container>
        </Layout>
    );
}

var Container = styled.div`
    max-width: 1140px;
    margin: 0 auto;
    padding-top: 50px;
`;

export async function getStaticPaths () {
    var projects = await fetchProjects();

    var paths = projects.map(project => ({ params: { id: project.id } }));

    return { paths, fallback: false };
}

export async function getStaticProps ({ params: { id }}) {
    var project = await fetchProjectById(id);

    return {
        props: {
            project
        }
    };
}